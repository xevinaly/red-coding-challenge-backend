﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using API.Enums;
using API.Interfaces;
using API.Models;
using API.Projections;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Area("Order")]
    [Route("[controller]")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [EnableCors("ReactFrontend")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService dataProvider;

        public OrderController(IOrderService dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        [HttpGet]
        public IActionResult Get(string OrderType, string CustomerName) => Ok(dataProvider.GetData(OrderType, CustomerName));

        [HttpGet("{Id}")]
        public IActionResult Get(string Id)
        {
            try
            {
                var order = dataProvider.GetData(int.Parse(Id));

                if (order != null)
                    return Ok(order);
                return NotFound();
            }
            catch
            {
                return BadRequest();
            }

        }

        // Add a Search endpoint

        [HttpPost]
        public IActionResult Post(Orders Order)
        {
            if (Order == null || Order.Customer == null || Order.Creator == null)
                return BadRequest();
            return Created("/order", dataProvider.CreateOrder(Order.Type, Order.Customer, Order.Creator));
        }

        //I've deviated from the assignment for this API because the provided request did not adhere to RESTful standards
        //Delete endpoints should only apply to a single resource, not multiple
        //To delete multiple resources the client should make a call to each resource individually
        //The backend structure you provided reflects this fact by only allowing a single delete per call in the service layer
        [HttpDelete("{Id}")]
        public IActionResult Delete(string Id)
        {
            try
            {
                if (dataProvider.DeleteData(int.Parse(Id)))
                    return Ok();
                return NotFound();
            }
            catch
            {
                return BadRequest();
            }
            
        }
    }
}