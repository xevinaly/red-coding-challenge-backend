﻿using API.Enums;
using API.Interfaces;
using API.Models;
using API.Projections;
using System.Collections.Generic;
using System;
using Telerik.JustMock;
using Xunit.Abstractions;
using Xunit;
using System.Linq;
using API.Services;

namespace API.Test.Services
{
    public class DatabaseFixture : IDisposable
    {
        public List<Orders> Orders { get; private set; }

        public DatabaseFixture()
        {
            Orders = new List<Orders>
            {
                new Orders
                {
                    Id = 0,
                    Customer = "Kroger",
                    Type = (int)OrderType.SaleOrder,
                    Created = DateTime.Now,
                    Creator = "Jenny"
                },
                new Orders
                {
                    Id = 1,
                    Customer = "Walmart",
                    Type = (int)OrderType.Standard,
                    Created = DateTime.Now,
                    Creator = "Haeley"
                },
                new Orders
                {
                    Id = 2,
                    Customer = "Target",
                    Type = (int)OrderType.TransferOrder,
                    Created = DateTime.Now,
                    Creator = "Noor"
                },
                new Orders
                {
                    Id = 3,
                    Customer = "Starbucks",
                    Type = (int)OrderType.PurchaseOrder,
                    Created = DateTime.Now,
                    Creator = "Zyra"
                },
                new Orders
                {
                    Id = 4,
                    Customer = "Kohls",
                    Type = (int)OrderType.ReturnOrder,
                    Created = DateTime.Now,
                    Creator = "Alice"
                }
            };
        }

        public void Dispose() { }
    }
    public class TestOrderService : IClassFixture<DatabaseFixture>
    {
        private readonly DatabaseFixture database;
        private readonly ITestOutputHelper output;

        public TestOrderService(DatabaseFixture database, ITestOutputHelper output)
        {
            this.database = database;
            this.output = output;
        }

        [Fact]
        public void GetData_ShouldHaveRepositoryFetchOrders()
        {
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.GetData())
              .Returns(database.Orders.Select(o => new Order(o)));

            var service = new OrderService(mockOrderRepository);

            service.GetData(null, null);

            Mock.Assert(() => mockOrderRepository.GetData(), Occurs.Once());
        }

        [Fact]
        public void GetData_ShouldReturnRepositoryOrders()
        {
            var expected = database.Orders.Select(o => new Order(o));
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.GetData())
              .Returns(expected);

            var service = new OrderService(mockOrderRepository);

            var result = service.GetData(null, null);

            Assert.NotNull(result);
            Assert.Equal(5, result.Count());

            var expectedVsResult = expected.Zip(result, (expected, result) => expected.ToString().Equals(result.ToString()));

            foreach (var comparison in expectedVsResult)
                Assert.True(comparison);
        }

        [Fact]
        public void GetData_ShouldFilterByCustomerNameWhenSpecified()
        {
            var expected = database.Orders.Select(o => new Order(o));
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.GetData())
              .Returns(expected);

            var service = new OrderService(mockOrderRepository);

            var result = service.GetData(null, "Kroger");

            Assert.NotNull(result);
            Assert.Single(result);
            Assert.Equal(new Order(database.Orders.FirstOrDefault()).ToString(), result.FirstOrDefault().ToString());
        }

        [Fact]
        public void GetData_ShouldFilterByUserTypeWhenSpecified()
        {
            var expected = database.Orders.Select(o => new Order(o));
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.GetData())
              .Returns(expected);

            var service = new OrderService(mockOrderRepository);

            var result = service.GetData(OrderType.SaleOrder.ToString(), null);

            Assert.NotNull(result);
            Assert.Single(result);
            Assert.Equal(new Order(database.Orders.FirstOrDefault()).ToString(), result.FirstOrDefault().ToString());
        }

        [Fact]
        public void GetData_HaveTheRepositoryFetchTheSpecifiedOrder()
        {
            var expected = new Order(new Orders
            {
                Id = 1,
                Creator = "Jane",
                Created = DateTime.Now,
                Customer = "Target",
                Type = 1
            });
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.GetData(1))
              .Returns(expected);

            var service = new OrderService(mockOrderRepository);

            service.GetData(1);

            Mock.Assert(() => mockOrderRepository.GetData(1), Occurs.Once());
        }

        [Fact]
        public void GetData_ShouldReturnTheSpecifiedOrderFromRepository()
        {
            var expected = new Order(new Orders
            {
                Id = 1,
                Creator = "Jane",
                Created = DateTime.Now,
                Customer = "Target",
                Type = 1
            });
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.GetData(1))
              .Returns(expected);

            var service = new OrderService(mockOrderRepository);

            var result = service.GetData(1);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void CreateOrder_ShouldReturnTheCreatedOrder()
        {
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.CreateOrder(1, "Target", "Ricky"))
              .Returns(new Order(database.Orders.First()));

            var service = new OrderService(mockOrderRepository);

            var result = service.CreateOrder(1, "Target", "Ricky");

            Assert.IsType<Order>(result);
        }

        [Fact]
        public void CreateOrder_ShouldHaveTheRepositoryCreateTheOrder()
        {
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.CreateOrder(1, "Target", "Ricky"))
              .Returns(new Order(database.Orders.First()));

            var service = new OrderService(mockOrderRepository);

            service.CreateOrder(1, "Target", "Ricky");

            Mock.Assert(() => mockOrderRepository.CreateOrder(1, "Target", "Ricky"), Occurs.Once());
        }

        [Fact]
        public void CreateOrder_ShouldReturnTheOrderCreatedByTheRepository()
        {
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.CreateOrder(1, "Target", "Ricky"))
              .Returns(new Order(database.Orders.First()));

            var service = new OrderService(mockOrderRepository);

            var result = service.CreateOrder(1, "Target", "Ricky");

            Assert.Equal(new Order(database.Orders.First()).ToString(), result.ToString());
        }

        [Fact]
        public void DeleteData_ShouldReturnDeletionSuccessStatusFromRepository()
        {
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.DeleteData(1))
              .Returns(true);

            var service = new OrderService(mockOrderRepository);

            var result = service.DeleteData(1);

            Assert.True(result);
        }

        [Fact]
        public void DeleteData_ShouldHaveTheRepositoryDeleteTheOrder()
        {
            var mockOrderRepository = Mock.Create<IOrderRepository>();
            Mock.Arrange(() => mockOrderRepository.DeleteData(1))
              .Returns(true);

            var service = new OrderService(mockOrderRepository);

            service.DeleteData(1);

            Mock.Assert(() => mockOrderRepository.DeleteData(1), Occurs.Once());
        }
    }
}