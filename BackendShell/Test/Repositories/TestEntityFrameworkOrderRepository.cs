﻿using API.Enums;
using API.Interfaces;
using API.Models;
using API.Projections;
using System.Collections.Generic;
using System;
using Xunit.Abstractions;
using Xunit;
using Microsoft.EntityFrameworkCore;
using API.Contexts;
using API.Repository;
using System.Linq;

namespace API.Test.Repositories
{
    public class DatabaseFixture : IDisposable
    {
        public List<Orders> Orders { get; private set; }

        public DatabaseFixture()
        {
            Orders = new List<Orders>
            {
                new Orders
                {
                    Customer = "Kroger",
                    Type = (int)OrderType.SaleOrder,
                    Creator = "Jenny"
                },
                new Orders
                {
                    Customer = "Walmart",
                    Type = (int)OrderType.Standard,
                    Creator = "Haeley"
                },
                new Orders
                {
                    Customer = "Target",
                    Type = (int)OrderType.TransferOrder,
                    Creator = "Noor"
                },
                new Orders
                {
                    Customer = "Starbucks",
                    Type = (int)OrderType.PurchaseOrder,
                    Creator = "Zyra"
                },
                new Orders
                {
                    Customer = "Kohls",
                    Type = (int)OrderType.ReturnOrder,
                    Creator = "Alice"
                },
            };
        }

        public void Dispose() { }
    }
    public class TestEntityFrameworkOrderRepository : IClassFixture<DatabaseFixture>
    {
        private readonly DatabaseFixture database;
        private readonly ITestOutputHelper output;

        public TestEntityFrameworkOrderRepository(DatabaseFixture database, ITestOutputHelper output)
        {
            this.database = database;
            this.output = output;
        }

        [Fact]
        public void GetData_ShouldReturnAllContextOrdersProjected()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "GetData_ShouldReturnAllContextOrdersProjected")
                .Options;

            using (var context = new OrderContext(options))
            {
                foreach (var order in database.Orders)
                    context.Orders.Add(order);

                context.SaveChanges();
            }

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.GetData();

                Assert.NotNull(result);
                Assert.Equal(5, result.Count());

                var expected = context.Orders.ToList().Select(o => new Order(o));
                var expectedVsResult = expected.Zip(result, (expected, result) => expected.ToString().Equals(result.ToString()));

                foreach (var comparison in expectedVsResult)
                    Assert.True(comparison);
            }
        }

        [Fact]
        public void GetData_ShouldReturnTheOrderSpecifiedById()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "GetData_ShouldReturnTheOrderSpecifiedById")
                .Options;

            using (var context = new OrderContext(options))
            {
                foreach (var order in database.Orders)
                    context.Orders.Add(order);

                context.SaveChanges();
            }

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.GetData(1);

                Assert.NotNull(result);
                Assert.Equal(new Order(context.Orders.Find(1)).ToString(), result.ToString());
            }
        }

        [Fact]
        public void GetData_ShouldReturnNullIfTheOrderSpecifiedByIdDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "GetData_ShouldReturnNullIfTheOrderSpecifiedByIdDoesNotExist")
                .Options;

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.GetData(1);

                Assert.Null(result);
            }
        }

        [Fact]
        public void CreateOrder_ShouldAddOrderToContext()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "CreateOrder_ShouldAddOrderToContext")
                .Options;

            using (var context = new OrderContext(options))
            {
                Assert.Empty(context.Orders.ToList());

                var repository = new EntityFrameworkOrderRepository(context);

                repository.CreateOrder(1, "Target", "Ricky");

                Assert.Single(context.Orders.ToList());
            }
        }

        [Fact]
        public void CreateOrder_ShouldReturnProjectedOrder()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "CreateOrder_ShouldReturnProjectedOrder")
                .Options;

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.CreateOrder(1, "Target", "Ricky");

                Assert.IsType<Order>(result);
            }
        }

        [Fact]
        public void CreateOrder_ShouldCreateReturnedOrderFromParameters()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "CreateOrder_ShouldCreateReturnedOrderFromParameters")
                .Options;

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.CreateOrder(1, "Target", "Ricky");

                Assert.Equal("Target", result.CustomerName);
                Assert.Equal("Ricky", result.CreatedByUserName);
                Assert.Equal(((OrderType)1).ToString(), result.OrderType);
            }
        }

        [Fact]
        public void CreateOrder_ShouldAddReturnedOrder()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "CreateOrder_ShouldAddReturnedOrder")
                .Options;

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.CreateOrder(1, "Target", "Ricky");

                var expected = new Order(context.Orders.OrderByDescending(o => o.Id).FirstOrDefault());

                Assert.Equal(expected.CustomerName, result.CustomerName);
                Assert.Equal(expected.CreatedByUserName, result.CreatedByUserName);
                Assert.Equal(expected.OrderType, result.OrderType);
            }
        }

        [Fact]
        public void DeleteData_ShouldRemoveAnOrderWhenTheSpecifiedIdIsInUse()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "DeleteData_ShouldRemoveAnOrderWhenTheSpecifiedIdIsInUse")
                .Options;

            using (var context = new OrderContext(options))
            {
                foreach (var order in database.Orders)
                    context.Orders.Add(order);

                context.SaveChanges();
            }

            using (var context = new OrderContext(options))
            {
                Assert.Equal(5, context.Orders.ToList().Count);

                var repository = new EntityFrameworkOrderRepository(context);

                repository.DeleteData(1);

                Assert.Equal(4, context.Orders.ToList().Count);
            }
        }

        [Fact]
        public void DeleteData_ShouldReturnTrueWhenTheSpecifiedIdIsInUse()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "DeleteData_ShouldReturnTrueWhenTheSpecifiedIdIsInUse")
                .Options;

            using (var context = new OrderContext(options))
            {
                foreach (var order in database.Orders)
                    context.Orders.Add(order);

                context.SaveChanges();
            }

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.DeleteData(1);

                Assert.True(result);
            }
        }

        [Fact]
        public void DeleteData_ShouldRemoveTheOrderSpecifiedById()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "DeleteData_ShouldRemoveTheOrderSpecifiedById")
                .Options;

            using (var context = new OrderContext(options))
            {
                foreach (var order in database.Orders)
                    context.Orders.Add(order);

                context.SaveChanges();
            }

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                repository.DeleteData(1);

                Assert.Null(context.Orders.Find(1));
            }
        }

        [Fact]
        public void DeleteData_ShouldDoNothingWhenTheSpecifiedIdIsNotInUse()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "DeleteData_ShouldDoNothingWhenTheSpecifiedIdIsNotInUse")
                .Options;

            using (var context = new OrderContext(options))
            {
                foreach (var order in database.Orders)
                    context.Orders.Add(order);

                context.SaveChanges();
            }

            using (var context = new OrderContext(options))
            {
                Assert.Equal(5, context.Orders.ToList().Count);

                var repository = new EntityFrameworkOrderRepository(context);

                repository.DeleteData(100);

                Assert.Equal(5, context.Orders.ToList().Count);
            }
        }

        [Fact]
        public void DeleteData_ShouldReturnFalseWhenTheSpecifiedIdIsNotInUse()
        {
            var options = new DbContextOptionsBuilder<OrderContext>()
                .UseInMemoryDatabase(databaseName: "DeleteData_ShouldReturnFalseWhenTheSpecifiedIdIsNotInUse")
                .Options;

            using (var context = new OrderContext(options))
            {
                foreach (var order in database.Orders)
                    context.Orders.Add(order);

                context.SaveChanges();
            }

            using (var context = new OrderContext(options))
            {
                var repository = new EntityFrameworkOrderRepository(context);

                var result = repository.DeleteData(100);

                Assert.False(result);
            }
        }
    }
}