﻿using API.Projections;
using Xunit.Abstractions;
using Xunit;
using API.Models;
using API.Enums;
using System;

namespace API.Test.Projections
{
    public class TestOrder
    {
        private readonly ITestOutputHelper output;

        public TestOrder(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Constructor_ShouldMapFieldsOfOrdersAsPrimitives()
        {
            var orders = new Orders
            {
                Id = 0,
                Customer = "Kroger",
                Type = (int)OrderType.SaleOrder,
                Created = DateTime.Now,
                Creator = "Jenny"
            };

            var order = new Order(orders);

            Assert.Equal(order.OrderId, orders.Id);
            Assert.Equal(order.CreatedByUserName, orders.Creator);
            Assert.Equal(order.CustomerName, orders.Customer);
            Assert.Equal(order.CreatedDate, orders.Created.ToLongDateString());
            Assert.Equal(order.OrderType, ((OrderType)orders.Type).ToString());
        }

        [Fact]
        public void ToString_ShouldConcatenateAllFields()
        {
            var orders = new Orders
            {
                Id = 0,
                Customer = "Kroger",
                Type = (int)OrderType.SaleOrder,
                Created = DateTime.Now,
                Creator = "Jenny"
            };

            var order = new Order(orders);

            Assert.Equal(order.ToString(), order.OrderId + " " + order.OrderType + " " + order.CustomerName + " " + order.CreatedDate + " " + order.CreatedByUserName);
        }
    }
}
