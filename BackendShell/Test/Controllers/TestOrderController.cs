﻿using API.Controllers;
using API.Enums;
using API.Interfaces;
using API.Models;
using API.Projections;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telerik.JustMock;
using Telerik.JustMock.Helpers;
using Xunit;
using Xunit.Abstractions;

namespace API.Test.Controllers
{
    public class DatabaseFixture : IDisposable
    {
        public List<Orders> Orders { get; private set; }

        public DatabaseFixture()
        {
            Orders = new List<Orders>
            {
                new Orders
                {
                    Id = 0,
                    Customer = "Kroger",
                    Type = (int)OrderType.SaleOrder,
                    Created = DateTime.Now,
                    Creator = "Jenny"
                },
                new Orders
                {
                    Id = 1,
                    Customer = "Walmart",
                    Type = (int)OrderType.Standard,
                    Created = DateTime.Now,
                    Creator = "Haeley"
                },
                new Orders
                {
                    Id = 2,
                    Customer = "Target",
                    Type = (int)OrderType.TransferOrder,
                    Created = DateTime.Now,
                    Creator = "Noor"
                },
                new Orders
                {
                    Id = 3,
                    Customer = "Starbucks",
                    Type = (int)OrderType.PurchaseOrder,
                    Created = DateTime.Now,
                    Creator = "Zyra"
                },
                new Orders
                {
                    Id = 4,
                    Customer = "Kohls",
                    Type = (int)OrderType.ReturnOrder,
                    Created = DateTime.Now,
                    Creator = "Alice"
                }
            };
        }

        public void Dispose() { }
    }
    public class TestOrderController : IClassFixture<DatabaseFixture>
    {
        private readonly DatabaseFixture database;
        private readonly ITestOutputHelper output;

        public TestOrderController(DatabaseFixture database, ITestOutputHelper output)
        {
            this.database = database;
            this.output = output;
        }

        [Fact]
        public void Get_ShouldHaveServiceFetchOrders()
        {
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.GetData("OrderType", "CustomerName"))
              .Returns(database.Orders.Select(o => new Order(o)));

            var controller = new OrderController(mockOrderService);

            controller.Get("OrderType", "CustomerName");

            Mock.Assert(() => mockOrderService.GetData("OrderType", "CustomerName"), Occurs.Once());
        }

        [Fact]
        public void Get_ShouldReturnAllServiceOrders()
        {
            var expected = database.Orders.Select(o => new Order(o));
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.GetData(null, null))
              .Returns(expected);

            var controller = new OrderController(mockOrderService);

            var result = controller.Get(null, null) as OkObjectResult;

            Assert.NotNull(result);

            var data = result.Value as IEnumerable<Order>;

            Assert.NotNull(data);
            Assert.Equal(5, data.Count());

            var expectedVsResult = expected.Zip(data, (expected, result) => expected.ToString().Equals(result.ToString()));

            foreach (var comparison in expectedVsResult)
                Assert.True(comparison);
        }

        [Fact]
        public void Get_ShouldHaveTheServiceFetchTheOrder()
        {
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.GetData(1))
              .Returns((Order)null);

            var controller = new OrderController(mockOrderService);

            controller.Get("1");

            Mock.Assert(() => mockOrderService.GetData(1), Occurs.Once());
        }

        [Fact]
        public void Get_ShouldReturnOkIfTheOrderExists()
        {
            var expected = new Order(new Orders
            {
                Id = 1,
                Creator = "Jane",
                Created = DateTime.Now,
                Customer = "Target",
                Type = 1
            });
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.GetData(1))
              .Returns(expected);

            var controller = new OrderController(mockOrderService);

            var result = controller.Get("1") as OkObjectResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void Get_ReturnTheOrderFromTheService()
        {
            var expected = new Order(new Orders
            {
                Id = 1,
                Creator = "Jane",
                Created = DateTime.Now,
                Customer = "Target",
                Type = 1
            });
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.GetData(1))
              .Returns(expected);

            var controller = new OrderController(mockOrderService);

            var result = controller.Get("1") as OkObjectResult;

            Assert.NotNull(result);

            var data = result.Value as Order;

            Assert.NotNull(data);
            Assert.Equal(expected, data);
        }

        [Fact]
        public void Get_ShouldReturnNotFoundIfTheOrderDoesNotExist()
        {
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.GetData(1))
              .Returns((Order)null);

            var controller = new OrderController(mockOrderService);

            var result = controller.Get("1") as NotFoundResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void Get_ShouldReturnBadRequestIfTheIdIsNotANumber()
        {
            var mockOrderService = Mock.Create<IOrderService>();

            var controller = new OrderController(mockOrderService);

            var result = controller.Get("A") as BadRequestResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void CreateOrder_ShouldReturnTheCreatedOrder()
        {
            var input = new Orders
            {
                Customer = "Target",
                Type = 1,
                Creator = "Ricky"
            };
            var expected = new Order(input);
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.CreateOrder(1, "Target", "Ricky"))
              .Returns(expected);

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(input) as CreatedResult;

            Assert.NotNull(result);

            var data = result.Value as Order;

            Assert.NotNull(data);
            Assert.Equal(expected.CustomerName, data.CustomerName);
            Assert.Equal(expected.OrderType, data.OrderType);
            Assert.Equal(expected.CreatedByUserName, data.CreatedByUserName);
        }

        [Fact]
        public void CreateOrder_ShouldHaveTheServiceCreateTheOrder()
        {
            var expected = new Orders
            {
                Customer = "Target",
                Type = 1,
                Creator = "Ricky"
            };
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.CreateOrder(1, "Target", "Ricky"))
              .Returns(new Order(expected));

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(expected) as CreatedResult;

            var data = result.Value as Order;

            Mock.Assert(() => mockOrderService.CreateOrder(1, "Target", "Ricky"), Occurs.Once());
        }

        [Fact]
        public void CreateOrder_ShouldReturnBadRequestWhenOrderIsNotProvided()
        {
            var mockOrderService = Mock.Create<IOrderService>();

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(null) as BadRequestResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void CreateOrder_ShouldReturnBadRequestWhenCreatorIsNotProvided()
        {
            var expected = new Orders
            {
                Customer = "Target",
                Type = 1
            };
            var mockOrderService = Mock.Create<IOrderService>();

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(expected) as BadRequestResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void CreateOrder_ShouldReturnBadRequestWhenCustomerIsNotProvided()
        {
            var expected = new Orders
            {
                Type = 1,
                Creator = "Ricky"
            };
            var mockOrderService = Mock.Create<IOrderService>();

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(expected) as BadRequestResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void CreateOrder_ShouldDefaultToStandardOrderIfTypeIsNotProvided()
        {
            var input = new Orders
            {
                Customer = "Target",
                Creator = "Ricky"
            };
            var expected = new Order(input);
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.CreateOrder(0, "Target", "Ricky"))
              .Returns(expected);

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(input) as CreatedResult;

            Assert.NotNull(result);

            var data = result.Value as Order;

            Assert.NotNull(data);
            Assert.Equal(((OrderType)0).ToString(), data.OrderType);
        }

        [Fact]
        public void CreateOrder_ShouldIgnoreOrderIdPassedIn()
        {
            var input = new Orders
            {
                Id = 100,
                Customer = "Target",
                Type = 1,
                Creator = "Ricky"
            };
            var expected = new Order(input);
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.CreateOrder(1, "Target", "Ricky"))
              .Returns(new Order(new Orders
              {
                  Id = 1,
                  Customer = "Target",
                  Type = 1,
                  Creator = "Ricky"
              }));

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(input) as CreatedResult;

            Assert.NotNull(result);

            var data = result.Value as Order;

            output.WriteLine(data.ToString());

            Assert.NotNull(data);
            Assert.NotEqual(expected.OrderId, data.OrderId);
        }

        [Fact]
        public void CreateOrder_ShouldIgnoreOrderCreatedPassedIn()
        {
            var input = new Orders
            {
                Customer = "Target",
                Type = 1,
                Created = DateTime.MinValue,
                Creator = "Ricky"
            };
            var expected = new Order(input);
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.CreateOrder(1, "Target", "Ricky"))
              .Returns(new Order(new Orders
              {
                  Customer = "Target",
                  Type = 1,
                  Created = DateTime.Now,
                  Creator = "Ricky"
              }));

            var controller = new OrderController(mockOrderService);

            var result = controller.Post(input) as CreatedResult;

            Assert.NotNull(result);

            var data = result.Value as Order;

            Assert.NotNull(data);
            Assert.NotEqual(expected.CreatedDate, data.CreatedDate);
        }

        [Fact]
        public void DeleteData_ShouldHaveTheServiceDeleteTheOrder()
        {
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.DeleteData(1))
              .Returns(true);

            var controller = new OrderController(mockOrderService);

            var result = controller.Delete("1") as OkResult;

            Mock.Assert(() => mockOrderService.DeleteData(1), Occurs.Once());
        }

        [Fact]
        public void DeleteData_ShouldReturnOkIfTheOrderExists()
        {
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.DeleteData(1))
              .Returns(true);

            var controller = new OrderController(mockOrderService);

            var result = controller.Delete("1") as OkResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void DeleteData_ShouldReturnNotFoundIfTheOrderDoesNotExist()
        {
            var mockOrderService = Mock.Create<IOrderService>();
            Mock.Arrange(() => mockOrderService.DeleteData(1))
              .Returns(false);

            var controller = new OrderController(mockOrderService);

            var result = controller.Delete("1") as NotFoundResult;

            Assert.NotNull(result);
        }

        [Fact]
        public void DeleteData_ShouldReturnBadRequestIfTheIdIsNotANumber()
        {
            var mockOrderService = Mock.Create<IOrderService>();

            var controller = new OrderController(mockOrderService);

            var result = controller.Delete("A") as BadRequestResult;

            Assert.NotNull(result);
        }
    }
}
