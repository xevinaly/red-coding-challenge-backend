﻿using API.Enums;
using API.Interfaces;
using MySqlConnector;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using API.Projections;
using System.Linq;

namespace API.Services
{
    public class OrderService : IOrderService
    {
        private IOrderRepository orderRepository;
        public OrderService(IOrderRepository OrderRepository)
        {
            orderRepository = OrderRepository;
        }

        public Order CreateOrder(int Type, string Customer, string Creator)
        {
            return orderRepository.CreateOrder(Type, Customer, Creator);
        }

        public bool DeleteData(int Id)
        {
            return orderRepository.DeleteData(Id);
        }

        public Order GetData(int Id)
        {
            return orderRepository.GetData(Id);
        }

        public IEnumerable<Order> GetData(string Type, string CustomerName)
        {
            var orders = orderRepository.GetData().ToList<Order>();

            if (Type != null)
                orders = orders.Where(o => o.OrderType == Type).ToList<Order>();

            if (CustomerName != null)
                orders = orders.Where(o => o.CustomerName == CustomerName).ToList<Order>();

            return orders;
        }
    }
}
