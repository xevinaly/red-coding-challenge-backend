﻿using API.Interfaces;
using System.Collections.Generic;
using API.Contexts;
using System.Linq;
using API.Projections;
using API.Models;

namespace API.Repository
{
    public class EntityFrameworkOrderRepository : IOrderRepository
    {
        private OrderContext context;

        public EntityFrameworkOrderRepository(OrderContext Context)
        {
            context = Context;
        }

        public Order CreateOrder(int Type, string Customer, string Creator)
        {
            context.Orders.Add(new Orders
            {
                Customer = Customer,
                Type = Type,
                Creator = Creator
            });
            context.SaveChanges();

            return new Order(context.Orders.OrderByDescending(o => o.Id).FirstOrDefault());
        }

        public bool DeleteData(int Id)
        {
            var order = context.Orders.Find(Id);

            if (order != null)
            {
                context.Remove(order);
                context.SaveChanges();

                return true;
            }

            return false;
        }

        public Order GetData(int Id)
        {
            var order = context.Orders.Find(Id);

            return order != null ? new Order(order) : null;
        }

        public IEnumerable<Order> GetData()
        {
            var orders = context.Orders.ToList();

            return orders.Select(o => new Order(o));
        }
    }
}

