﻿using API.Enums;
using API.Projections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Interfaces
{
    public interface IOrderRepository
    {
        public IEnumerable<Order> GetData();

        public Order GetData(int Id);

        public Order CreateOrder(int OrderType, string CustomerName, string CreatedByUserName);

        public bool DeleteData(int Id);
    }
}