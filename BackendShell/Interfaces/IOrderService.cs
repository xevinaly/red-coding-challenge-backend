﻿using API.Enums;
using API.Projections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Interfaces
{
    public interface IOrderService
    {
        public IEnumerable<Order> GetData(string Type, string CustomerName);

        public Order GetData(int Id);

        public Order CreateOrder(int OrderType, string CustomerName, string CreatedByUserName);

        public bool DeleteData(int Id);
    }
}