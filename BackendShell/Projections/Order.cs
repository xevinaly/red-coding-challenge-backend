﻿using API.Enums;
using API.Models;

namespace API.Projections
{
    public class Order
    {
        public Order(Orders o)
        {
            OrderId = o.Id;
            CreatedByUserName = o.Creator;
            CustomerName = o.Customer;
            CreatedDate = o.Created.ToLongDateString();
            OrderType = ((OrderType)o.Type).ToString();
        }

        public int OrderId { get; set; }
        public string OrderType { get; set; }
        public string CustomerName { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedByUserName { get; set; }

        public override string ToString()
        {
            return OrderId + " " + OrderType + " " + CustomerName + " " + CreatedDate +  " " + CreatedByUserName;
        }
    }
}