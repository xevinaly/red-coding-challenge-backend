﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    public partial class Orders
    {
        public int Id { get; set; }
        public string Customer { get; set; }
        public int Type { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Created { get; set; }
        public string Creator { get; set; }
    }
}
